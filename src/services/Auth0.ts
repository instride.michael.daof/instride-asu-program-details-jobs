import axios from "axios";
import Config from "./Config";

const {
  auth0Audience,
  auth0ClientId,
  auth0ClientSecret,
  auth0Issuer
} = Config;

export const generateToken = async () => {
  const response = await axios.post(
    `${auth0Issuer}/oauth/token`,
    {
      audience: auth0Audience,
      client_id: auth0ClientId,
      client_secret: auth0ClientSecret,
      grant_type: "client_credentials"
    },
    {
      headers: { "Content-Type": "application/json" }
    }
  );

  const { access_token } = response.data!;

  if (!access_token) {
    throw new Error("Unable to get access token");
  };

  return access_token;
}

export default {
  generateToken,
}