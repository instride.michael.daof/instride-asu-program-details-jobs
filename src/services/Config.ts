import dotenv from 'dotenv';

dotenv.config();

export interface EnvVariables {
  asuApiKey: string;
  asuApiUrl: string,
  auth0Audience: string,
  auth0ClientId: string,
  auth0ClientSecret: string,
  auth0Domain: string,
  auth0Issuer: string,
  coreApiUrl: string,
}

const {
  ASU_API_KEY,
  ASU_API_URL,
  AUTH0_AUDIENCE,
  AUTH0_CLIENT_ID,
  AUTH0_CLIENT_SECRET,
  AUTH0_DOMAIN,
  AUTH0_ISSUER,
  CORE_API_URL,
} = process.env;

const envVar: EnvVariables = {
  asuApiKey: ASU_API_KEY,
  asuApiUrl: ASU_API_URL,
  auth0Audience: AUTH0_AUDIENCE,
  auth0ClientId: AUTH0_CLIENT_ID,
  auth0ClientSecret: AUTH0_CLIENT_SECRET,
  auth0Domain: AUTH0_DOMAIN,
  auth0Issuer: AUTH0_ISSUER,
  coreApiUrl: CORE_API_URL
}

export default envVar;

