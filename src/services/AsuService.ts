import axios, { AxiosResponse } from 'axios'
import Config from './Config'

const {
  asuApiKey,
  asuApiUrl
} = Config

const allProgramsQuery = `
  query AllProgramsQuery {
    allPrograms {
      id,
      detailPage,
      code,
      programCode,
      planCode,
      subPlanCode,
      title,
      alternateTitle,
      shortDescription,
      longDescription,
      weeksPerClass,
      totalClasses,
      totalCreditHours,
      nextStartDate,
      category,
      degreeImage,
      interestAreas
    }
  }
`

const nodeProgramQuery = `
query ProgramQuery($id: Int!) {
  program(id: $id) {
      id,
      code,
      programCode,
      planCode,
      subPlanCode,
      concentrationCode,
      title,
      alternateTitle,
      shortDescription,
      longDescription.
      detailPage,
      weeksPerClass,
      totalClasses,
      totalCreditHours,
      nextStartDate,
      metaTags {
          title,
          description
      },
      certificateType,
      pathway,
      prestigiousFaculty {
          title,
          description,
          image
      },
      youtubeVideo {
          title,
          description,
          embedUrl
      },
      category,
      degreeImage,
      interestAreas,
      cipCode,
      majorMapUrl,
      featuredCourses {
          title,
          description,
          items {
              title,
              description,
              category,
              courseSearchUrl
          }
      },
      relatedCareers {
          title,
          description,
          image,
          items {
              title,
              salary
          }
      },
      accolades {
          title,
          description,
          items {
              title,
              description
          }
      },
      admissionRequirements {
          title,
          description,
          items {
              title
          }
      },
      cost {
          manual {
              amount
          }
      },
      faq {
          title,
          items {
              title
          }
      },
      interestedPrograms {
          title
      }
  }
}
`

export const getPrograms = async () => {
  return axios
    .post(asuApiUrl, {
      query: allProgramsQuery
    }, {
      headers: {
        'x-api-key': asuApiKey,
        'content-type': 'application/json',
      }
    })
    .catch(e => {
      throw 'ASU API request failed'
    })
}
