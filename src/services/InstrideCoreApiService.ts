import axios from 'axios'
import Auth0 from './Auth0'
import Config from './Config'

enum CoreApiEndpoints {
  PROGRAMS_BULK = '/v1/programs/bulk',
}

const {
  coreApiUrl
} = Config;

export const updateInstrideProgramsBulk = async (programs) => {
  try {
    const token = await Auth0.generateToken();

    return axios.put(`${coreApiUrl}${CoreApiEndpoints}`, programs, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
  } catch (error) {
    console.log(error.response.data);
    throw error;
  }
}

export default {
  updateInstrideProgramsBulk,
}