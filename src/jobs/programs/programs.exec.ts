import axios from 'axios'
import { programsJob } from './programs.job';

export const programsConfig = {
  name: 'programs',
  display: 'Update ASU Programs',
  export: true
}

const setHttpMiddleware = (axios) => {
  axios.interceptors.request.use(request => {
    console.log(`${programsConfig.display}`, request)
    return request
  })
  
  axios.interceptors.response.use(response => {
    console.log(`${programsConfig.display}`, response)
    return response
  })
}

export const programsExecutor = async (options) => {
  console.log(`Starting job: ${programsConfig.display}`);
  console.time(programsConfig.name);
  try {
    await programsJob(options);
  } catch(e) {
    console.error('Programs Job Failed\n', e)
  }
  console.log(`${programsConfig.display} Complete`);
  console.timeEnd(programsConfig.name);
};

setHttpMiddleware(axios);
