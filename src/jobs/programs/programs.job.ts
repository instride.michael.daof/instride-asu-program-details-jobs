import { get } from 'lodash'
import { getPrograms } from '../../services/AsuService'
import { format } from 'date-fns'
import { updateInstrideProgramsBulk } from '../../services/InstrideCoreApiService'
import path from 'path'
import fs from 'fs'

const modelAsuProgramToInstrideProgram = (asuProgram) => {
  return asuProgram
}

const exportToJson = (data) => {
  const timestamp = format(new Date(), 'MM-dd-yyyy--HH-mm');
  const fileLocation = path.join(
    __dirname,
    `../../../output/${timestamp}.json`
  );
  fs.writeFile(fileLocation, JSON.stringify(data), err => {
    if (err) {
      console.log(err);
    } else {
      console.log("\x1b[32m%s\x1b[0m", `CSV file stored in: ${fileLocation}`);
    }
  });
}

export const programsJob = async (options?: any) => {
  const apiResponse = await getPrograms();
  const data = get(apiResponse, ['data', 'data', 'allPrograms'], []);
  const instridePrograms = data.map(modelAsuProgramToInstrideProgram);
  if (options?.export) {
    await exportToJson(instridePrograms)
  } else {
    await updateInstrideProgramsBulk(instridePrograms);
  }
}