# ASU Jobs

Run each job discretely from its own run file.

## Programs

To run Programs job:
```
npm run job:programs
```
or
```
node dist/programs.run
```

Requires environment variables for ASU and Instride Core API authentication in `.env` file (see `env.example`):
```
ASU_API_KEY=<secret-key>
ASU_API_URL=https://phat-graphql.edpl.us/graphql
AUTH0_AUDIENCE=dev-instride.com/api/corporate
AUTH0_CLIENT_ID=<ID>
AUTH0_CLIENT_SECRET=<secret>
AUTH0_DOMAIN=dev-instride.auth0.com
AUTH0_ISSUER=https://dev-instride.auth0.com
CORE_API_URL=http://localhost:3000
```
